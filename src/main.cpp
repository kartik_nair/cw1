/*
 * util.cpp
 * Author: M00697094
 * Created: 14/1/2021
 * Last Updated: 20/1/2021
 */

#include <iomanip>
#include <iostream>
#include <vector>

#include "csv.h"
#include "fs.h"
#include "item.h"
#include "items.h"
#include "program.h"
#include "util.h"

int main() {
    clear();

    static std::vector<Item*> local_store;
    static std::vector<Sale> sales_report;

    print(R"(
   ____                  __               
  /  _/__ _  _____ ___  / /____  ______ __
 _/ // _ \ |/ / -_) _ \/ __/ _ \/ __/ // /
/___/_//_/___/\__/_//_/\__/\___/_/  \_, / 
                                   /___/
)");

    try {
        std::string file_content = fs::read_file("stock-data.csv");
        local_store = csv::parse_items(file_content);
    } catch (const fs::error& error) {
        print(std::cerr, error.what());
        println(
            "\n\nHad error while reading file (see above for "
            "details).\n\nLocal store has been kept blank & the program should "
            "work as expected.\n");
    } catch (const csv::error& error) {
        print(std::cerr, error.what());
        println(
            "\n\nHad error while parsing stock-data.csv file (see above for "
            "details).\n\nLocal store has been kept blank & the program should "
            "work as expected.\n");
    }

    println("\nSelect an option to continue:");

    while (true) {
        auto choice = prompt<int>(
            "  1. Sell items\n"
            "  2. Restock items\n"
            "  3. Add new items\n"
            "  4. Update stock quantity\n"
            "  5. View sales report\n"
            "  6. Save inventory changes to file\n"
            "  7. Load stock data from file\n"
            "  8. Quit program\n\n",
            [](int selection) { return (selection >= 1 && selection <= 8); },
            "[Error] please try again with a whole number between 1-8: ",
            false);

        clear();

        /*
            using if-else instead of switch-case as mentioned
            in the code guidelines for better readability
        */
        if (choice == 1) {
            sell_items(local_store, sales_report);
        } else if (choice == 2) {
            restock_items(local_store, sales_report);
        } else if (choice == 3) {
            add_new_item(local_store, sales_report);
        } else if (choice == 4) {
            update_quantity(local_store, sales_report);
        } else if (choice == 5) {
            view_sales_report(local_store, sales_report);
        } else if (choice == 6) {
            save_items_to_file(local_store, sales_report, "stock-data.csv");
        } else if (choice == 7) {
            load_stock_data(local_store, sales_report);
        } else if (choice == 8) {
            clear_vector_of_pointers(local_store);
            println("Thank you for using inventory!");
            break;
        }
    }

    return EXIT_SUCCESS;
}