#include "items.h"

/*
 * items.cpp
 * Author: M00697094
 * Created: 14/1/2021
 * Last Updated: 14/1/2021
 */

#include <sstream>

#include "item.h"

/*
    ----
     CD
    ----
*/

/**
 * @brief Construct a new CD object as a subclass of Item.
 *
 * @see Item::Item for other paramaters.
 * @param album The album that this CD is a part of.
 * @param artist The artist(s) who worked on this CD.
 * @param genre The genre this CD can be categorized as.
 */
CD::CD(int id, string_ref title, double count, double price, string_ref album,
       string_ref artist, string_ref genre)
    : Item(id, title, count, price, CATEGORY_CD),
      album(album),
      artist(artist),
      genre(genre) {}

/**
 * @brief The CSV header for all CD objects, similar to table headers.
 */
std::string CD::csv_header = "ID,Title,Count,Price,Album,Arist,Genre";

/**
 * @brief Get a CSV representation of the CD object. Useful for printing &
 * debugging but also to serialize and in files.
 *
 * @return std::string The CSV string (without headers).
 */
std::string CD::to_csv() {
    std::ostringstream output;

    output << id << ',' << title << ',' << count << ',' << price << ',' << album
           << ',' << artist << ',' << genre;

    return output.str();
}

/*
    -----
     DVD
    -----
*/

/**
 * @brief Construct a new DVD object as a subclass of Item.
 *
 * @see Item::Item & CD::CD for other paramaters.
 * @param producer The team/person responsible for producing the video.
 */
DVD::DVD(int id, string_ref title, double count, double price, string_ref album,
         string_ref artist, string_ref genre, string_ref producer)
    : CD(id, title, count, price, album, artist, genre), producer(producer) {
    // change the category as CD initializes it as `CATEGORY_CD`
    this->category = CATEGORY_DVD;
}

/**
 * @brief The CSV header for all DVD objects, similar to table headers.
 */
std::string DVD::csv_header = "ID,Title,Count,Price,Album,Arist,Genre,Producer";

/**
 * @brief Get a CSV representation of the DVD object. Useful for printing &
 * debugging but also to serialize and in files.
 *
 * @return std::string The CSV string (without headers).
 */
std::string DVD::to_csv() {
    std::ostringstream output;

    output << id << ',' << title << ',' << count << ',' << price << ',' << album
           << ',' << artist << ',' << genre << ',' << producer;

    return output.str();
}

/*
    ------
     Book
    ------
*/

/**
 * @brief Construct a new Book object
 *
 * @see Item::Item for other parameters.
 * @param author The author(s) who wrote the book.
 * @param year_published The year this book was published.
 * @param ISBN The International Standard Book Number (either 10 or 13) for this
 * book.
 */
Book::Book(int id, string_ref title, double count, double price,
           string_ref author, int year_published, string_ref ISBN)
    : Item(id, title, count, price, CATEGORY_BOOK),
      ISBN(ISBN),
      author(author),
      year_published(year_published) {}

/**
 * @brief The CSV header for all Book objects, similar to table headers.
 */
std::string Book::csv_header =
    "ID,Title,Count,Price,Author,Year Published,ISBN";

/**
 * @brief Get a CSV representation of the Book object. Useful for printing &
 * debugging but also to serialize and in files.
 *
 * @return std::string The CSV string (without headers).
 */
std::string Book::to_csv() {
    std::ostringstream output;

    output << id << ',' << title << ',' << count << ',' << price << ','
           << author << ',' << year_published << ',' << ISBN;

    return output.str();
}

/*
    ----------
     Magazine
    ----------
*/

/**
 * @brief Construct a new Magazine object
 *
 * @param author The author(s) who worked on this issue of the magazine.
 * @param issue_details The issue details (e.g. issue number/volume number).
 * @param ISSN The International Standard Serial Number for this issue of the
 * magazine.
 */
Magazine::Magazine(int id, string_ref title, double count, double price,
                   string_ref author, string_ref issue_details, string_ref ISSN)
    : Item(id, title, count, price, CATEGORY_MAGAZINE),
      author(author),
      ISSN(ISSN),
      issue_details(issue_details) {}

/**
 * @brief The CSV header for all Magazine objects, similar to table headers.
 */
std::string Magazine::csv_header =
    "ID,Title,Count,Price,Author,Issue Details,ISSN";

/**
 * @brief Get a CSV representation of the Magazine object. Useful for printing &
 * debugging but also to serialize and in files.
 *
 * @return std::string The CSV string (without headers).
 */
std::string Magazine::to_csv() {
    std::ostringstream output;

    output << id << ',' << title << ',' << count << ',' << price << ','
           << author << ',' << issue_details << ',' << ISSN;

    return output.str();
}
