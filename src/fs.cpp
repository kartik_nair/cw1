#include "fs.h"

/*
 * fs.cpp
 * Author: M00697094
 * Created: 15/1/2021
 * Last Updated: 15/1/2021
 */

#include <cerrno>
#include <cstring>
#include <exception>
#include <fstream>
#include <iostream>
#include <sstream>

#include "util.h"

/**
 * @brief Reads a file & returns the content as a string. Handles error by
 * printing the error code (+ explanation) & then exiting.
 *
 * @param path The path for the file to be read.
 * @return std::string The contents of the file.
 */
std::string fs::read_file(string_ref path) {
    std::ifstream input_file(path);

    if (input_file) {
        std::stringstream buffer;
        buffer << input_file.rdbuf();
        return buffer.str();
    } else {
        throw fs::error(
            format("[Error] File ({}) could not be opened while attempting to "
                   "read.\nError code: [{}] {}",
                   path, errno, std::strerror(errno)));
    }
}

/**
 * @brief Writed a string to a file. Handles error by printing the error code (+
 * explanation) & then exiting.
 *
 * @param path The path of the file which is to be written to.
 * @param content The content that has to be written.
 */
void fs::write_file(string_ref path, string_ref content) {
    std::ofstream output_file(path);
    if (!output_file) {
        throw fs::error(
            format("[Error] File ({}) could not be opened while attempting to "
                   "write.\nError code: [{}] {}",
                   path, errno, std::strerror(errno)));
    }
    print(output_file, content);  // make use of our flexible print function
}