#include "item.h"

/*
 * item.cpp
 * Author: M00697094
 * Created: 14/1/2021
 * Last Updated: 14/1/2021
 */

/**
 * @brief Constructs a new Item object, not meant to be used directly. Only
 * meant to be used by subclasses.
 *
 * @param id The ID of the item to be created.
 * @param title The title, i.e. what this item should be called.
 * @param count The current qunatity of this item.
 * @param price The price that this item sells at.
 * @param category What category this item is (only meant to be initialized by
 * subclasses).
 */
Item::Item(int id, string_ref title, double count, double price,
           Category category)
    : id(id), title(title), count(count), price(price), category(category) {}