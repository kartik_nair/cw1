#include "program.h"

#include <chrono>
#include <ctime>
#include <iomanip>
#include <iostream>
#include <limits>
#include <vector>

#include "csv.h"
#include "fs.h"
#include "item.h"
#include "items.h"
#include "util.h"

void print_table(const std::vector<Item*>& items) {
    const std::string LINE =
        "+---------------------------------"
        "----------------------------------+";

    std::cout << LINE << "\n|" << std::setw(4) << "ID"
              << " | " << std::left << std::setw(24) << "TITLE" << std::right
              << " |" << std::setw(6) << "COUNT"
              << " | " << std::left << std::setw(12) << "CATEGORY" << std::right
              << " |" << std::setw(10) << "PRICE"
              << " |\n"
              << LINE << std::endl;

    for (Item* item : items)
        std::cout << '|' << std::setw(4) << item->id << " | " << std::left
                  << std::setw(24) << item->title << std::right << " |"
                  << std::setw(6) << item->count << " | " << std::left
                  << std::setw(12) << CATEGORY_LABELS[item->category]
                  << std::right << " |" << std::setw(10) << item->price
                  << " |\n"
                  << LINE << std::endl;
}

Item* select_item(const std::vector<Item*>& items, string_ref prompt_string) {
    print_table(items);

    auto id = prompt<int>(
        prompt_string,
        [items](int input_id) {
            for (Item* item : items) {
                if (item->id == input_id) {
                    return true;
                }
            }
            // if the input id is not found in the store
            return false;
        },
        "\n[Error] That ID is not a part of the table. Please "
        "try again with a valid ID\n\n");

    for (Item* item : items) {
        if (item->id == id) return item;
    }
}

void sell_items(std::vector<Item*>& items, std::vector<Sale>& sales_report) {
    Item* item_to_sell = select_item(
        items, "Please input the ID of the item you would like to sell: ");

    auto quantity = prompt<int>(
        "Please input the quantity of items you would like to "
        "sell: ");

    // store a backup of the previous count for error handling
    int prev_count = item_to_sell->count;
    item_to_sell->count -= quantity;

    if (item_to_sell->count < 0) {
        clear();
        println(
            "\n[Error] Chosen quantity was greater than "
            "available stock. Please try again.\n");
        item_to_sell->count = prev_count;
        sell_items(items, sales_report);
    } else {
        sales_report.push_back(
            Sale{std::chrono::system_clock::now(), item_to_sell, quantity});

        clear();
        println("\n[Success] {} items have been sold successfully!\n",
                quantity);
        println("Updated count for '{}' is now {}", item_to_sell->title,
                item_to_sell->count);
    }
}

void restock_items(std::vector<Item*>& items, std::vector<Sale>& sales_report) {
    Item* item_to_restock = select_item(
        items, "Please input the ID of the item you would like to restock: ");

    auto quantity = prompt<int>(
        "Please input the number of items you would like to "
        "add to the stock: ");

    item_to_restock->count += quantity;

    clear();
    println("\n[Success] {} items have been restocked successfully!\n",
            quantity);
    println("Updated count for '{}' is now {}", item_to_restock->title,
            item_to_restock->count);
}

void add_new_item(std::vector<Item*>& items, std::vector<Sale>& sales_report) {
    auto type_of_item = prompt<int>(
        "Please select the type of item you would like to add:\n\n"
        "1. Book\n2. Magazine\n3. CD\n4. DVD\n\n",
        [](int chosen_type) { return chosen_type >= 1 && chosen_type <= 4; },
        "Please make sure your input is a whole number between 1-4.");

    auto id = prompt<int>(
        "Unique ID for your item: ",
        [items](int given_id) {
            for (Item* item : items) {
                if (item->id == given_id) {
                    return false;
                }
            }
            return true;
        },
        "Please make sure your input id is a whole number & not "
        "already in use.\n");

    auto title = prompt<std::string>("The title of the item: ");
    auto count = prompt<double>("The initial quantity for this item: ");
    auto price = prompt<double>("The price that this item will sell at: ");

    if (type_of_item == 1) {
        auto ISBN = prompt<std::string>("The ISBN for your book: ");
        auto author = prompt<std::string>("The book's author: ");
        auto year_published =
            prompt<int>("The year this book was published in: ");

        items.push_back(
            new Book(id, title, count, price, author, year_published, ISBN));
    } else if (type_of_item == 2) {
        auto author = prompt<std::string>(
            "The author of this magazine (individual or organization): ");
        auto ISSN = prompt<std::string>("The ISSN for your magazine: ");
        auto issue_details =
            prompt<std::string>("Issue details for this specific issue: ");

        items.push_back(
            new Magazine(id, title, count, price, author, issue_details, ISSN));
    } else if (type_of_item == 3 || type_of_item == 4) {
        auto album =
            prompt<std::string>("The album that this disk is a part of: ");
        auto artist = prompt<std::string>(
            "The primary artist(s) who worked on this disk: ");
        auto genre =
            prompt<std::string>("The genre that this disk belongs in: ");

        if (type_of_item == 4) {
            auto producer = prompt<std::string>(
                "The person/organization responsible for producing the video "
                "on this DVD: ");
            items.push_back(new DVD(id, title, count, price, album, artist,
                                    genre, producer));
        } else {
            items.push_back(
                new CD(id, title, count, price, album, artist, genre));
        }
    }
    clear();
    println("\n[Success] Added new item '{}' to store.\n", items.back()->title);
}

void update_quantity(std::vector<Item*>& items,
                     std::vector<Sale>& sales_report) {
    Item* item_to_update = select_item(
        items,
        "Please input the ID of the item whose quantity you would like to "
        "update: ");

    int new_count = prompt<int>(
        "Please input the new quantity for this item: ",
        [](int input_count) { return input_count > 0; },
        "Please make sure your input is a whole number greater than 0.\n");

    item_to_update->count = new_count;

    clear();
    println("\n[Success] Updated count for '{}' is now {}\n",
            item_to_update->title, item_to_update->count);
}

void view_sales_report(std::vector<Item*>& items,
                       std::vector<Sale>& sales_report) {
    clear();

    println("\nSales report: ");
    if (sales_report.size() > 0) {
        double total_revenue = 0;

        for (const Sale& action : sales_report) {
            std::time_t unix_timestamp =
                std::chrono::system_clock::to_time_t(action.timestamp);
            println("\n{} | Sold {} of '{}'", std::ctime(&unix_timestamp),
                    action.quantity_sold, action.item_sold->title);
            total_revenue += action.item_sold->price * action.quantity_sold;
        }

        println("\nTotal revenue for sales: {}", total_revenue);
    } else {
        println("\nNo sales made yet.");
    }

    print("\nPress <ENTER> to go back: ");
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    clear();
}

void save_items_to_file(std::vector<Item*>& items,
                        std::vector<Sale>& sales_report, string_ref path) {
    std::string items_csv = csv::stringify_items(items);
    try {
        fs::write_file(path, items_csv);
        println("\n[Success] Saved local changes to file!\n");
    } catch (const fs::error& error) {
        print(std::cerr, error.what());
        exit(EXIT_FAILURE);
    }
}

void load_stock_data(std::vector<Item*>& items,
                     std::vector<Sale>& sales_report) {
    auto path =
        prompt<std::string>("Please input the path for the file to load: ");

    try {
        std::string file_contents = fs::read_file(path);
        std::vector<Item*> parsed_items = csv::parse_items(file_contents);
        clear_vector_of_pointers(items);
        items = parsed_items;
    } catch (const fs::error& error) {
        print(std::cerr, error.what());
        exit(EXIT_FAILURE);
    }
}