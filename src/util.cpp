#include "util.h"

/*
 * util.cpp
 * Author: M00697094
 * Created: 14/1/2021
 * Last Updated: 19/1/2021
 */

#include <iostream>
#include <vector>

void clear() {
    // clear the using system commands (cross-platform)
#if defined(WIN32) || defined(_WIN32) || \
    defined(__WIN32) && !defined(__CYGWIN__)
    system("cls");
#else
    system("clear");
#endif
}