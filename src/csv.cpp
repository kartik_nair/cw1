#include "csv.h"

/*
 * csv.cpp
 * Author: M00697094
 * Created: 14/1/2021
 * Last Updated: 19/1/2021
 */

#include <iostream>
#include <sstream>

#include "item.h"
#include "items.h"
#include "util.h"

/**
 * @brief Parses a string of CSV into a 2D vector of string. Tested to be
 * compliant with RFC 4180 (https://tools.ietf.org/html/rfc4180).
 *
 * @param csv_string The string to be parsed.
 * @return std::vector<std::vector<std::string>> A 2D vector of string. The
 * first dimension is for each row & then one string per field.
 */
std::vector<std::vector<std::string>> csv::parse(string_ref csv_string) {
    const std::vector<std::string> EMPTY_LINE = {""};
    std::vector<std::vector<std::string>> result;

    std::string string_buffer;
    std::vector<std::string> line_buffer;

    bool in_string;
    size_t max_len = csv_string.size(), index = 0;

    while (index < max_len) {
        if (csv_string[index] == ',') {
            if (in_string) {
                // commas can be used when enclosed by double quotes.
                string_buffer += ',';
            } else {
                line_buffer.push_back(string_buffer);
                string_buffer = "";
            }
        } else if (csv_string[index] == '"') {
            /*
             * double quotes can be used when enclosed by double
             * quotes only if paired with another double quote.
             *
             * For example: one,"two,s","""three's!""
             * should parse to: {"one", "two,s", "\"three's\""}
             */

            if (in_string && csv_string[index + 1] == '"') {
                string_buffer += '"';
                index++;
            } else if (in_string && csv_string[index + 1] != '"') {
                in_string = false;
            } else if (!in_string) {
                in_string = true;
            }
        } else if (csv_string[index] == '\n') {
            if (in_string) {
                string_buffer += '\n';  // add the line break to the string
            }
            // if the '\n' is at the end we can ignore it
            else if (index != max_len - 1) {
                line_buffer.push_back(string_buffer);

                // if the line buffer has only one empty string element we can
                // assume that the line was just a CRLF or LF. in that case we
                // don't need to add the line to the result.
                if (line_buffer != EMPTY_LINE && !line_buffer.empty()) {
                    result.push_back(line_buffer);
                }

                string_buffer = "";
                line_buffer.clear();
            }
        } else if (csv_string[index] == '\r') {
            if (in_string) {
                // carriage returns are only significant if they are
                // enclosed in double quotes
                string_buffer += '\r';
            }

        } else {
            string_buffer += csv_string[index];
        }

        index++;
    }

    // the last elements won't be pushed in the loop. so:
    line_buffer.push_back(string_buffer);
    result.push_back(line_buffer);

    return result;
}

/**
 * @brief Utility function to check if a vector has only one element & the
 * element is equal to the provided value.
 *
 * @param vec The vector.
 * @param str The value to check.
 */
inline bool check_label(const std::vector<std::string> &vec, string_ref str) {
    if (vec.size() == 1 && vec[0] == str) {
        return true;
    } else {
        return false;
    }
}

/**
 * @brief Parses a CSV string into Items. The type of item is determined by
 * checking for Category Headers (like "Books:", "CDs:", etc.)
 *
 * @param csv_string The string to parsed.
 * @return std::vector<Item *> The vector of Items (pointers for polymorphism).
 */
std::vector<Item *> csv::parse_items(string_ref csv_string) {
    std::vector<Item *> result;
    std::vector<std::vector<std::string>> parsed = csv::parse(csv_string);

    size_t number_of_rows = parsed.size();

    size_t i = 0;
    Category current_category;

    while (i < number_of_rows) {
        if (check_label(parsed[i], "Books:")) {
            current_category = CATEGORY_BOOK;
            i += 2;  // to skip both the category header & the table
                     // headers in the next line
        } else if (check_label(parsed[i], "CDs:")) {
            current_category = CATEGORY_CD;
            i += 2;
        } else if (check_label(parsed[i], "DVDs:")) {
            current_category = CATEGORY_DVD;
            i += 2;
        } else if (check_label(parsed[i], "Magazines:")) {
            current_category = CATEGORY_MAGAZINE;
            i += 2;
        }

        if (i >= number_of_rows) {
            /*
                should throw as this would mean the category
                header was used but without any fields after
            */
            throw csv::error("[Error] Expected row. {}");
        }

        std::ostringstream row_stream;
        row_stream << "[";
        size_t last = parsed[i].size() - 1;
        for (size_t j = 0; j < last + 1; j++) {
            row_stream << parsed[i][j];
            if (j != last) row_stream << ", ";
        }
        row_stream << "]";

        try {
            if (parsed[i].size() != 7 &&
                !(current_category == CATEGORY_DVD && parsed[i].size() == 8)) {
                throw csv::error(
                    format("[Error] Incorrect number of fields for Item: "
                           "{}. Size was {}",
                           row_stream.str(), parsed[i].size()));
            }

            int id = std::stoi(parsed[i][0]);
            double count = std::stod(parsed[i][2]);
            double price = std::stod(parsed[i][3]);

            if (current_category == CATEGORY_BOOK) {
                int year = std::stoi(parsed[i][5]);
                result.push_back(new Book(id, parsed[i][1], count, price,
                                          parsed[i][4], year, parsed[i][6]));
            } else if (current_category == CATEGORY_CD) {
                result.push_back(new CD(id, parsed[i][1], count, price,
                                        parsed[i][4], parsed[i][5],
                                        parsed[i][6]));
            } else if (current_category == CATEGORY_DVD) {
                result.push_back(new DVD(id, parsed[i][1], count, price,
                                         parsed[i][4], parsed[i][5],
                                         parsed[i][6], parsed[i][7]));
            } else if (current_category == CATEGORY_MAGAZINE) {
                result.push_back(new Magazine(id, parsed[i][1], count, price,
                                              parsed[i][4], parsed[i][5],
                                              parsed[i][6]));
            }  // ignore all lines that are not in any section
        } catch (const std::invalid_argument &error) {
            throw csv::error(
                format("[Error] Item had invalid type for fields. While "
                       "parsing row: {}",
                       row_stream.str()));
        }

        i++;
    }

    return result;
}

/**
 * @brief Converts a vector of (pointers to) Items into valid (category
 * seperated) CSV.
 *
 * @param items The vector of (pointers to) Items.
 * @return std::string The CSV generated by converting the Items.
 */
std::string csv::stringify_items(std::vector<Item *> items) {
    std::string item_strs[4] = {"", "", "", ""};
    if (items.size() == 0) return "";

    for (Item *item : items) {
        item_strs[item->category] += item->to_csv() + "\n";
    }

    std::ostringstream output;

    for (size_t i = 0; i < 4; i++) {
        if (item_strs[i] != "") {
            output << CATEGORY_LABELS[i] + "s:\n" << item_strs[i];
        }
    }

    return output.str();
}