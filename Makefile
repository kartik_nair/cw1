CXX=g++
CXXFLAGS=-Wall -Iinclude -std=c++17

SRCS=$(wildcard src/*.cpp)
OBJS=$(patsubst src/%.cpp, build/tmp/%.o, $(SRCS))
TESTS=$(wildcard test/*.cpp)
TEST_OBJS=$(patsubst test/%.cpp, build/tmp/test/%.o, $(TESTS))

run: all
	./build/bin/inventory

all: build/bin/inventory

build/bin/inventory: $(OBJS)
	@mkdir -p build/bin
	$(CXX) $(OBJS) -o build/bin/inventory

build/tmp/%.o: src/%.cpp
	@mkdir -p build/tmp
	$(CXX) $(CXXFLAGS) -c $< -o $@

test: all build/bin/tests

test-run: test
	./build/bin/tests

build/bin/tests: $(TEST_OBJS)
	$(CXX) $(filter-out build/tmp/main.o,$(OBJS)) $(TEST_OBJS) -o build/bin/tests

build/tmp/test/%.o: test/%.cpp
	@mkdir -p build/tmp/test
	$(CXX) $(CXXFLAGS) -c $< -o $@

clean:
	rm -rf build

.PHONY: all run clean test test-run