/*
 * test_csv.cpp
 * Author: M00697094
 * Brief: Non-Item based test cases are taken (almost) directly from RFC 4180
 * (https://tools.ietf.org/html/rfc4180) & it's specification of CSV.
 * Created: 14/1/2021
 * Last Updated: 16/1/2021
 */

#include <iostream>
#include <string>
#include <vector>

#include "../lib/catch.hpp"
#include "csv.h"
#include "item.h"
#include "items.h"
#include "util.h"

/* General purpose CSV parsing */

TEST_CASE("Basic CSV. Single record per line, no special characters.",
          "[csv::parse]") {
    std::string input = "aaa,bbb,ccc\nxxx,yyy,zzz";
    std::vector<std::vector<std::string>> expected_output = {
        {"aaa", "bbb", "ccc"}, {"xxx", "yyy", "zzz"}};
    std::vector parsed_output = csv::parse(input);

    REQUIRE(parsed_output == expected_output);
}

TEST_CASE("Basic CSV with CRLF.", "[csv::parse]") {
    std::string input = "aaa,bbb,ccc\r\nxxx,yyy,zzz";
    std::vector<std::vector<std::string>> expected_output = {
        {"aaa", "bbb", "ccc"}, {"xxx", "yyy", "zzz"}};
    REQUIRE(csv::parse(input) == expected_output);
}

TEST_CASE("Trailing line break or CRLF is optional", "[csv::parse]") {
    std::string traling_line_break = "aaa,bbb,ccc\nxxx,yyy,zzz\n";
    std::string trailing_crlf = "aaa,bbb,ccc\r\nxxx,yyy,zzz\r\n";
    std::vector<std::vector<std::string>> expected_output = {
        {"aaa", "bbb", "ccc"}, {"xxx", "yyy", "zzz"}};

    REQUIRE(csv::parse(traling_line_break) == expected_output);
    REQUIRE(csv::parse(trailing_crlf) == expected_output);
}

TEST_CASE("Whitespace is not ignored in fields.", "[csv::parse]") {
    std::string input = "aa aa,bbb bb,c ccc\nxxx,yyy,zzz";
    std::vector<std::vector<std::string>> expected_output = {
        {"aa aa", "bbb bb", "c ccc"}, {"xxx", "yyy", "zzz"}};

    REQUIRE(csv::parse(input) == expected_output);
}

TEST_CASE("Trailing comma is treated as a blank field.", "[csv::parse]") {
    std::string input = "aaa,bbb,ccc\nxxx,yyy,zzz,";
    std::vector<std::vector<std::string>> expected_output = {
        {"aaa", "bbb", "ccc"}, {"xxx", "yyy", "zzz", ""}};

    REQUIRE(csv::parse(input) == expected_output);
}

TEST_CASE(
    "Enclosing fields in double quotes allows for escaping line-breaks, more "
    "double quotes, and commas.",
    "[csv::parse]") {
    std::string basic = "\"aaa\",\"bbb\",\"ccc\"\nxxx,\"yyy\",zzz";
    std::vector<std::vector<std::string>> expected_output_basic = {
        {"aaa", "bbb", "ccc"}, {"xxx", "yyy", "zzz"}};

    std::string with_line_breaks = "\"a\naa\",\"b\nb\nb\",ccc\nxxx,yyy,zzz";
    std::string with_crlfs = "\"a\r\naa\",\"b\r\nb\r\nb\",ccc\r\nxxx,yyy,zzz";
    std::vector<std::vector<std::string>> expected_output_with_line_breaks = {
        {"a\naa", "b\nb\nb", "ccc"}, {"xxx", "yyy", "zzz"}};
    std::vector<std::vector<std::string>> expected_output_with_crlfs = {
        {"a\r\naa", "b\r\nb\r\nb", "ccc"}, {"xxx", "yyy", "zzz"}};

    std::string with_commas = "\"aa,a\",\"b,b,b\",ccc\n\"x,xx\",yyy,zzz";
    std::vector<std::vector<std::string>> expected_output_with_commas = {
        {"aa,a", "b,b,b", "ccc"}, {"x,xx", "yyy", "zzz"}};

    std::string with_escaped_double_quoted =
        "\"a\"\"a\"\"a\",bbb,\"\"\"ccc\"\"\"\nxxx,yyy,zzz";
    std::vector<std::vector<std::string>>
        expected_output_with_escaped_double_quoted = {
            {"a\"a\"a", "bbb", "\"ccc\""}, {"xxx", "yyy", "zzz"}};

    REQUIRE(csv::parse(basic) == expected_output_basic);
    REQUIRE(csv::parse(with_line_breaks) == expected_output_with_line_breaks);
    REQUIRE(csv::parse(with_crlfs) == expected_output_with_crlfs);
    REQUIRE(csv::parse(with_commas) == expected_output_with_commas);
    REQUIRE(csv::parse(with_escaped_double_quoted) ==
            expected_output_with_escaped_double_quoted);
}

TEST_CASE("Lines that only have CRLF or LF are ignored.", "[csv::parse]") {
    std::string with_line_breaks = "aaa,bbb,ccc\n\n\n\nxxx,yyy,zzz";
    std::string with_crlfs = "aaa,bbb,ccc\r\n\r\n\r\n\r\nxxx,yyy,zzz";
    std::vector<std::vector<std::string>> expected_output = {
        {"aaa", "bbb", "ccc"}, {"xxx", "yyy", "zzz"}};

    REQUIRE(csv::parse(with_line_breaks) == expected_output);
    REQUIRE(csv::parse(with_crlfs) == expected_output);
}

TEST_CASE("Valid category seperated CSV is parsed.", "[csv::parse_items]") {
    std::string with_all_categories = R"(Books:
ID,Title,Count,Price,Author,Year Published,ISBN
0,test book,1,12.99,test author,2001,test isbn

Magazines:
ID,Title,Count,Price,Author,Issue Details,ISSN
1,test magazine,2,12.99,test author,test details,test issn

CDs:
ID,Title,Count,Price,Album,Arist,Genre
2,test cd,3,12.99,test album,test artist,test genre

DVDs:
ID,Title,Count,Price,Album,Arist,Genre,Producer
3,test dvd,4,12.99,test album,test artist,test genre,test producer)";

    std::string with_some_categories = R"(Books:
ID,Title,Count,Price,Author,Year Published,ISBN
0,test book,1,12.99,test author,2001,test isbn

Magazines:
ID,Title,Count,Price,Author,Issue Details,ISSN
3,test magazine,4,12.99,test author,test details,test issn)";

    std::vector<Item*> parsed;
    // shouldn't throw, since csv is valid
    REQUIRE_NOTHROW(parsed = csv::parse_items(with_all_categories));
    REQUIRE(parsed.size() == 4);

    // check if properties match up
    for (size_t i = 0; i < parsed.size(); i++) {
        REQUIRE((size_t)parsed[i]->category == i);
        REQUIRE((size_t)parsed[i]->id == i);
        REQUIRE(parsed[i]->count == (double)i + 1);
        REQUIRE(parsed[i]->price == 12.99);
    }

    clear_vector_of_pointers(parsed);
    REQUIRE_NOTHROW(parsed = csv::parse_items(with_some_categories));
    REQUIRE(parsed.size() == 2);

    // no need for further property checks since the previous test has already
    // confirmed that properties are parsed validly
    REQUIRE(parsed[0]->category == CATEGORY_BOOK);
    REQUIRE(parsed[1]->category == CATEGORY_MAGAZINE);
    clear_vector_of_pointers(parsed);
}

TEST_CASE("Invalid CSV for items throws a csv::error.", "[csv::parse_items]") {
    std::string missing_column_headers = R"(Books:
0,test book,1,12.99,test author,2001,test isbn)";

    std::string no_rows_for_category = R"(Books:
    ID,Title,Count,Price,Author,Year Published,ISBN

    Magazines:
    ID,Title,Count,Price,Author,Issue Details,ISSN
    3,test magazine,4,12.99,test author,test details,test issn)";

    // e.g. string for number
    std::string incorrect_type_of_field = R"(Magazines:
    ID,Title,Count,Price,Author,Issue Details,ISSN
    3,test magazine,4,invalid price,test author,test details,test issn)";

    std::vector<Item*> parsed;
    REQUIRE_THROWS_AS(parsed = csv::parse_items(missing_column_headers),
                      csv::error);
    clear_vector_of_pointers(parsed);
    REQUIRE_THROWS_AS(parsed = csv::parse_items(no_rows_for_category),
                      csv::error);
    clear_vector_of_pointers(parsed);
    REQUIRE_THROWS_AS(parsed = csv::parse_items(incorrect_type_of_field),
                      csv::error);
    clear_vector_of_pointers(parsed);
}

/* Converting Item pointers into CSV */

TEST_CASE("Items of different categories can be strinigified validly.",
          "[csv::stringify_items]") {
    std::vector<Item*> input;
    input.push_back(
        new Book(1, "test title", 1, 12.99, "test author", 2001, "test isbn"));

    // with single category
    REQUIRE(csv::stringify_items(input) ==
            "Books:\n1,test title,1,12.99,test author,2001,test isbn\n");

    input.push_back(new Magazine(2, "test title", 1, 12.99, "test author",
                                 "test details", "test issn"));

    // with two categories
    REQUIRE(csv::stringify_items(input) ==
            "Books:\n1,test title,1,12.99,test author,2001,test isbn\n"
            "Magazines:\n2,test title,1,12.99,test author,test details,test "
            "issn\n");

    clear_vector_of_pointers(input);
}