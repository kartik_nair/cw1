/*
 * test_util.cpp
 * Author: M00697094
 * Brief: User input & validation testing for all the functionalities.
 * Created: 20/1/2021
 * Last Updated: 21/1/2021
 */

#include <iostream>
#include <sstream>
#include <vector>

#include "../lib/catch.hpp"
#include "item.h"
#include "items.h"
#include "program.h"

static std::streambuf* original_cin = std::cin.rdbuf();
static std::streambuf* original_cout = std::cout.rdbuf();
static std::istringstream test_input;
static std::ostringstream test_output;
static std::vector<Item*> test_store;
static std::vector<Sale> test_sales_report;

TEST_CASE(
    "The sell items functionality validates input & modifies the store "
    "correctly.",
    "[sell_items]") {
    clear_vector_of_pointers(test_store);
    test_sales_report.clear();

    test_store.push_back(
        new Book(1, "test book", 10, 12.99, "test author", 2001, "test isbn"));

    /*
        tests invalid input (string instead of int)
        & illogical input (5 is not an id in the table)
    */
    test_input.str("string\r\n200\r\n1\r\nstring\r\n5\r\n");
    test_input.clear();

    std::cin.rdbuf(test_input.rdbuf());
    std::cout.rdbuf(test_output.rdbuf());

    REQUIRE_NOTHROW(sell_items(test_store, test_sales_report));

    std::cin.rdbuf(original_cin);
    std::cout.rdbuf(original_cout);

    REQUIRE(test_store[0]->count == 5);
    REQUIRE_THAT(
        test_output.str(),
        Catch::EndsWith(
            "[Success] 5 items have been sold successfully!\n\nUpdated count "
            "for 'test book' is now 5\n"));
}

TEST_CASE(
    "The restock items functionality validates input & modifies the store "
    "correctly.",
    "[restock_items]") {
    clear_vector_of_pointers(test_store);
    test_sales_report.clear();

    test_store.push_back(new Magazine(1, "test magazine", 10, 12.99,
                                      "test author", "test issue details",
                                      "test isbn"));

    /*
        tests invalid input (string instead of int)
        & illogical input (5 is not an id in the table)
    */
    test_input.str("string\r\n200\r\n1\r\nstring\r\n10\r\n");
    test_input.clear();

    std::cin.rdbuf(test_input.rdbuf());
    std::cout.rdbuf(test_output.rdbuf());

    REQUIRE_NOTHROW(restock_items(test_store, test_sales_report));

    std::cin.rdbuf(original_cin);
    std::cout.rdbuf(original_cout);

    REQUIRE(test_store[0]->count == 20);
    REQUIRE_THAT(test_output.str(),
                 Catch::EndsWith("[Success] 10 items have been restocked "
                                 "successfully!\n\nUpdated count "
                                 "for 'test magazine' is now 20\n"));
}

TEST_CASE(
    "The functionality to update item's quantities validates input & modifies "
    "the store correctly.",
    "[update_quantity]") {
    clear_vector_of_pointers(test_store);
    test_sales_report.clear();

    test_store.push_back(new CD(1, "test cd", 10, 12.99, "test album",
                                "test artist", "test genre"));

    /*
        tests invalid input (string instead of int)
        & illogical input (5 is not an id in the table)
    */
    test_input.str("string\r\n200\r\n1\r\nstring\r\n200\r\n");
    test_input.clear();

    std::cin.rdbuf(test_input.rdbuf());
    std::cout.rdbuf(test_output.rdbuf());

    REQUIRE_NOTHROW(update_quantity(test_store, test_sales_report));

    std::cin.rdbuf(original_cin);
    std::cout.rdbuf(original_cout);

    REQUIRE(test_store[0]->count == 200);
    REQUIRE_THAT(test_output.str(),
                 Catch::EndsWith(
                     "[Success] Updated count for 'test cd' is now 200\n\n"));
}

TEST_CASE(
    "The functionality to add a new item validates input & modifies the store "
    "correctly.",
    "[add_new_item]") {
    clear_vector_of_pointers(test_store);
    test_sales_report.clear();

    /* tests invalid input at all user input prompts. */
    test_input.str(
        "string\r\n4\r\nstring\r\n1\r\ntest dvd\r\n"
        "string\r\n20\r\nstring\r\n12.99\r\ntest album\r\n"
        "test artist\r\ntest genre\r\ntest producer\r\n");
    test_input.clear();
    test_output.str("");
    test_output.clear();

    std::cin.rdbuf(test_input.rdbuf());
    std::cout.rdbuf(test_output.rdbuf());

    REQUIRE_NOTHROW(add_new_item(test_store, test_sales_report));

    std::cin.rdbuf(original_cin);
    std::cout.rdbuf(original_cout);

    // checks if items was added
    REQUIRE(test_store.size() == 1);
}