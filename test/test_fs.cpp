/*
 * test_fs.cpp
 * Author: M00697094
 * Brief: Test cases for the filesystem library. Warning they make use of
 * `system` calls that only function on a *nix like shell.
 * Created: 16/1/2021
 * Last Updated: 16/1/2021
 */

#include <cstdlib>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>

#include "../lib/catch.hpp"
#include "fs.h"
#include "util.h"

TEST_CASE(
    "Writes string to file correctly (& creates file if file doesn't exist).",
    "[fs::write_file]") {
    std::string path = ".test_fs_write_temp.txt";
    std::string content = "Placeholder text.\nJust some testing text.\n";

    fs::write_file(path, content);

    std::ostringstream written_content;
    written_content << std::ifstream(path).rdbuf();

    REQUIRE(written_content.str() == content);

    // cleanup
    system(format("rm -rf {}", path).c_str());
}

TEST_CASE("Writing to a file without permission throws a fs::error",
          "[fs::write_file]") {
    std::string path = ".test_fs_no_write";
    // create the file & change the permissions to disable write privelages
    system(format("touch {} && chmod a-w {}", path, path).c_str());
    REQUIRE_THROWS_AS(fs::write_file(path, "\n"), fs::error);
    system(format("rm -rf {}", path).c_str());
}

TEST_CASE("Reads string from file correctly.", "[fs::read_file]") {
    std::string path = ".test_fs_read_temp.txt";
    std::string content = "Placeholder text.\\nJust some testing text.\\n";
    std::string unescaped_content =
        "Placeholder text.\nJust some testing text.\n";

    // writing into the file using printf & shell redirection
    system(format("printf \"{}\" > {}", content, path).c_str());
    std::string read_content = fs::read_file(path);

    REQUIRE(read_content == unescaped_content);
    system(format("rm -rf {}", path).c_str());
}

TEST_CASE("Reading from a file that does not exist throws a fs::error.",
          "[fs::read_file]") {
    REQUIRE_THROWS_AS(fs::read_file(".test_fs_nonexistent"), fs::error);
}