#ifndef _ITEM_H
#define _ITEM_H

/*
 * items.h
 * Author: M00697094
 * Brief: Abstract `Item` class.
 * Created: 14/1/2021
 * Last Updated: 16/1/2021
 */

#include <string>

#include "util.h"

/**
 * @brief Category Enum used to facilitate runtime polymorhpism. Allows us to
 * check what subclass we have from an Item pointer.
 */
enum Category {
    CATEGORY_BOOK,
    CATEGORY_MAGAZINE,
    CATEGORY_CD,
    CATEGORY_DVD,
};

const std::string CATEGORY_LABELS[] = {"Book", "Magazine", "CD", "DVD"};

/**
 * @brief Abstract base class. Houses shared properties & virtual methods.
 */
class Item {
   public:
    int id;
    std::string title;
    double count;
    double price;

    Category category;
    Item(int, string_ref, double, double, Category);
    virtual ~Item(){};
    virtual std::string to_csv() = 0;
};

#endif