#ifndef _CSV_H
#define _CSV_H

/*
 * csv.h
 * Author: M00697094
 * Brief: Contains the CSV namespace & function definitions within it.
 * Created: 14/1/2021
 * Last Updated: 15/1/2021
 */

#include <string>
#include <vector>

#include "item.h"
#include "items.h"
#include "util.h"

/**
 * @brief The CSV namespace includes both general-purpose & project-specific
 * functionality. Check the functions & their documentation to learn more where
 * which ones can be used.
 */
namespace csv {

// Method names are inspired by JavaScript's JSON api.
std::vector<std::vector<std::string>> parse(string_ref);
std::vector<Item *> parse_items(string_ref);
std::string stringify_items(std::vector<Item *>);

/**
 * @brief A minimal class that inherits from runtime_error to help distinguish
 * csv parsing & stringifying from other runtime exceptions.
 */
class error : public std::runtime_error {
    using std::runtime_error::runtime_error;  // similar to fs::error as well
};

}  // namespace csv

#endif