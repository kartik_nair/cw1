#ifndef _PROGRAM_H
#define _PROGRAM_H

/*
 * program.h
 * Author: M00697094
 * Brief: Includes all the user-facing program functions.
 * Created: 20/1/2021
 * Last Updated: 20/1/2021
 */

#include <chrono>
#include <ctime>

#include "item.h"

/**
 * @brief Holds a sale action. Is used for the sales report
 */
struct Sale {
    std::chrono::system_clock::time_point timestamp;
    Item* item_sold;
    int quantity_sold;
};

/**
 * @brief Prints a vector of items formatted as a single table for more readable
 * user output.
 *
 * @param items The vector of items.
 */
void print_table(const std::vector<Item*>&);

/**
 * @brief Shows the user the current store & prompts them to select an item by
 * its ID.
 *
 * @param prompt_string The string used when prompting the user.
 * @return Item* The Item that the user selected.
 */
Item* select_item(const std::vector<Item*>&, string_ref);

/**
 * @brief Allows user to sell a chosen amount of items from the store.
 */
void sell_items(std::vector<Item*>&, std::vector<Sale>&);

/**
 * @brief Allows user to increase the quantity of an item in the store by a
 * chosen amount.
 */
void restock_items(std::vector<Item*>&, std::vector<Sale>&);

/**
 * @brief Let's the user add a new item to the local store. Prompts are based on
 * the type of item they have chosen to add.
 */
void add_new_item(std::vector<Item*>&, std::vector<Sale>&);

/**
 * @brief Allows user to directly update the qunatity of a chosen item.
 */
void update_quantity(std::vector<Item*>&, std::vector<Sale>&);

/**
 * @brief Shows the user all the sales made by them & the total revenue earned
 * by those sales.
 */
void view_sales_report(std::vector<Item*>&, std::vector<Sale>&);

/**
 * @brief Saves the stores items to a file.
 *
 * @param path The location of the file where items should be saved.
 */
void save_items_to_file(std::vector<Item*>&, std::vector<Sale>&, string_ref);

/**
 * @brief Allows the user to load the local store from another file instead of
 * the default (stock-data.csv in the current directory).
 */
void load_stock_data(std::vector<Item*>&, std::vector<Sale>&);

#endif