#ifndef _FS_H
#define _FS_H

#include "util.h"

/*
 * fs.h
 * Author: M00697094
 * Brief: Contains the FS namespace & function definitions within it.
 * Created: 15/1/2021
 * Last Updated: 15/1/2021
 */

namespace fs {

std::string read_file(string_ref);
void write_file(string_ref, string_ref);

/**
 * @brief A minimal class that inherits from runtime_error to make fs-related
 * errors distinct from other runtime exceptions.
 */
class error : public std::runtime_error {
    // inherit runtime_error's constructors
    using std::runtime_error::runtime_error;
};

}  // namespace fs

#endif