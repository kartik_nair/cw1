#ifndef _UTIL_H
#define _UTIL_H

/*
 * util.h
 * Author: M00697094
 * Brief: Includes utility functions that have been used throughout the program.
 * Created: 14/1/2021
 * Last Updated: 16/1/2021
 */

#include <functional>
#include <iostream>
#include <sstream>
#include <string>
#include <type_traits>
#include <vector>

/**
 * @brief Clears the current terminal buffer using an escape sequence.
 */
void clear();

/**
 * @brief Frees all the memory allocated to a vector of pointers.
 *
 * @tparam T The type of the element to which pointers are being stored.
 * @param vec The vector of pointers.
 */
template <typename T>
void clear_vector_of_pointers(std::vector<T*>& vec) {
    for (T* item : vec) {
        delete item;
    }
    vec.clear();
}

/**
 * @brief Type alias for constant reference to string
 */
using string_ref = const std::string&;

/*
    -------------------
     String Formatting
    -------------------
*/

/**
 * @brief Writes one argument to a provided output stream. It also modifies the
 * value of current position so that future iterations can keep track of it.
 *
 * @tparam T The type of the argument to be written.
 * @param os The output stream that the argument should be written to.
 * @param format_str The format string (using "{}" to show where values have to
 * be interpolated).
 * @param current_pos The current position of iteration through the format
 * string.
 * @param arg The argument to be written.
 */
template <typename T>
inline void write_arg_to_stream(std::ostream& os, string_ref format_str,
                                std::size_t& current_pos, const T& arg) {
    std::size_t next_pos = format_str.find("{}", current_pos);

    // if the delimiter is not used just print the arguments (with spaces)
    if (next_pos == std::string::npos) {
        os.write(format_str.data() + current_pos,
                 format_str.size() - current_pos);
        current_pos = format_str.size();
        os << " ";
        os << arg;
    } else {
        // manipulating the c_string pointer for a faster result than substr
        os.write(format_str.data() + current_pos, next_pos - current_pos);
        os << arg;
        current_pos = next_pos + 2;
    }
}

/**
 * @brief Print but for ostreams other than std::cout.
 *
 * @tparam Args The types of the variadic arguments.
 * @param os The output stream to write to.
 * @param format_str The format string (using "{}" as delimiter).
 * @param args The arguments to be interpolated into the format string.
 */
template <typename... Args>
inline void print(std::ostream& os, string_ref format_str,
                  const Args&... args) {
    std::size_t current_pos = 0;

    /*
        C++ 17 introduced fold expressions
        https://en.cppreference.com/w/cpp/language/fold. This allows us to
        iterate through `args` without trying to deduct it's type in code.

        The line below calls the `write_arg_to_stream` function for each of the
        arguments that `println` was called.
     */
    (write_arg_to_stream(os, format_str, current_pos, args), ...);

    // if after manipulating the pointer for `format_str` it is still not empty,
    // write the left over string to cout
    if (!format_str.empty()) {
        os.write((&format_str.front()) + current_pos,
                 format_str.size() - current_pos);
    }
}

/**
 * @brief A variadic cli write function. Similar to Python & Rust, it
 * uses "{}" as a delimiter to denote where the variadic arguments should be
 * interpolated.
 *
 * @tparam Args The types of the variadic arguments.
 * @param format_str The format string (using "{}" as delimiter).
 * @param args The arguments to be interpolated into the format string.
 */
template <typename... Args>
inline void print(string_ref format_str, const Args&... args) {
    print(std::cout, format_str, args...);
}

/**
 * @brief Prints an empty new line to stdout.
 */
inline void println() { std::cout << std::endl; }

/**
 * @brief Println but for other ostreams except for std::cout.
 *
 * @tparam Args The types of the variadic arguments.
 * @param os The output stream to write to.
 * @param format_str The format string (using "{}" as delimiter).
 * @param args The arguments to be interpolated into the format string.
 */
template <typename... Args>
inline void println(std::ostream& os, string_ref format_str,
                    const Args&... args) {
    print(os, format_str, args...);
    os << std::endl;
}

/**
 * @brief A variadic cli write function. Similar to Python & Rust, it
 * uses "{}" as a delimiter to denote where the variadic arguments should be
 * interpolated. Also writes a trailing new line.
 *
 * @tparam Args The types of the variadic arguments.
 * @param format_str The format string (using "{}" as delimiter).
 * @param args The arguments to be interpolated into the format string.
 */
template <typename... Args>
inline void println(string_ref format_str, const Args&... args) {
    println(std::cout, format_str, args...);
}

/**
 * @brief Generates a formatted string with interpolated variables.
 *
 * @tparam Args The types of the variadic arguments.
 * @param format_str The format string (using "{}" as delimiter).
 * @param args The arguments to be interpolated into the format string.
 * @return std::string The formatted string with variables interpolated.
 */
template <typename... Args>
inline std::string format(string_ref format_str, const Args&... args) {
    std::ostringstream stream;
    print(stream, format_str, args...);
    return stream.str();
}

/*
    ------------
     User Input
    ------------
*/

/**
 * @brief Prompts the user for input & then returns the inputted value.
 *
 * @tparam T The type of the value you need from the user.
 * @param prompt_string The string to print when prompting the user.
 * @param validate The function used to verify whether the input is valid or not
 * (note that type invalidity is already handled, this should be used for
 * further validation of the value after type).
 * @param invalid_informer The string printed to the user that let's them know
 * their input was invalid.
 * @param reprint_prompt Whether or not the prompt should be reprinted when
 * reprompting the user.
 * @return T The value the user inputted.
 */
template <typename T>
inline T prompt(
    string_ref prompt_string,
    std::function<bool(T)> validate = [](T _) { return true; },
    string_ref invalid_informer = "That input is invalid. Please try again.\n",
    bool reprint_prompt = true) {
    T value;
    std::string str_value;
    std::cout << prompt_string;
    std::getline(std::cin, str_value);
    std::stringstream value_stream(str_value);

    /*
        using `std::ws` & `eof()` to make sure input is read till the
        end of the line. this avoids errors like `3.14` being valid for
        an `int` type because the string stream stopped reading after
        the `3`.
    */
    while (!(value_stream >> value && (value_stream >> std::ws).eof() &&
             validate(value))) {
        std::cout << invalid_informer;
        if (reprint_prompt) std::cout << prompt_string;
        std::getline(std::cin, str_value);
        /*
            resets value_stream's state completely & reinitializes
            it with the new value of str_value.
        */
        value_stream.str(str_value);
        value_stream.clear();
    }

    return value;
}

// specialization for strings (since no type validation is required)
template <>
inline std::string prompt(string_ref prompt_string,
                          std::function<bool(std::string)> validate,
                          string_ref invalid_informer, bool reprint_prompt) {
    std::string value;
    std::cout << prompt_string;

    while (std::getline(std::cin, value) && !validate(value)) {
        std::cout << invalid_informer;
        if (reprint_prompt) std::cout << prompt_string;
    }

    return value;
}

#endif