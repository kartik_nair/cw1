#ifndef _ITEMS_H
#define _ITEMS_H

/*
 * items.h
 * Author: M00697094
 * Brief: Subclasses for other items based on Item.
 * Created: 14/1/2021
 * Last Updated: 14/1/2021
 */

#include <string>

#include "item.h"
#include "util.h"

/**
 * @brief Subclass of Item for CDs.
 */
class CD : public Item {
   protected:
    std::string album;
    std::string artist;
    std::string genre;

   public:
    static std::string csv_header;
    CD(int, string_ref, double, double, string_ref, string_ref, string_ref);
    std::string to_csv();
};

/**
 * @brief Subclass of Item for DVDs.
 */
class DVD : public CD {
   private:
    std::string producer;

   public:
    static std::string csv_header;
    DVD(int, string_ref, double, double, string_ref, string_ref, string_ref,
        string_ref);
    std::string to_csv();
};

/**
 * @brief Subclass of Item for Books.
 */
class Book : public Item {
   private:
    std::string ISBN;
    std::string author;
    int year_published;

   public:
    static std::string csv_header;
    Book(int, string_ref, double, double, string_ref, int, string_ref);
    std::string to_csv();
};

/**
 * @brief Subclass of Item for Magazines.
 */
class Magazine : public Item {
   private:
    std::string author;
    std::string ISSN;
    std::string issue_details;

   public:
    static std::string csv_header;
    Magazine(int, string_ref, double, double, string_ref, string_ref,
             string_ref);
    std::string to_csv();
};

#endif