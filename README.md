# CST2550 Coursework 1

This is the repository for M00297094's coursework 1 for the CST2550 module. The project makes use of `make` to compile so please make sure you have that installed before checking the commands available below.

## Building

```shell
# to compile the program
make all

# to compile and run the program
make
# or
make run

# to compile test suite
make test

# to compile and run test cases
make test-run
```

## Documentation

The code for this project has been extensively documented using [Doxygen](https://www.doxygen.nl) style comments, which makes generating API documentation in markdown (or other formats) very simple.

## Stlye Guidelines

The code also follows the the style guidelines mentioned in the coursework brief very strongly, relying on both editor configuration and author decisions.
